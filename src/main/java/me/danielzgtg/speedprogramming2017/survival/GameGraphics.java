package me.danielzgtg.speedprogramming2017.survival;

import java.util.Random;
import java.util.function.IntConsumer;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

public final class GameGraphics implements KeyListener {
	private volatile BufferedImage frontBuffer;
	private BufferedImage backBuffer;
	private final JFrame frame;
	private Font font;
	public final Toolkit toolkit = Toolkit.getDefaultToolkit();
	public final int w;
	public final int h;
	private static final int FONT_SIZE = 12;
	public final boolean vsync;
	private boolean disposed = false;
	private static final IntConsumer EMPTY_LISTENER = (x) -> {};

	private IntConsumer keyDownListener = EMPTY_LISTENER;
	private IntConsumer keyUpListener = EMPTY_LISTENER;

	final BufferedImage getFrontBuffer() {
		return this.frontBuffer;
	}

	public final void setKeyDownListener(final IntConsumer listener) {
		if (listener == null) {
			throw new NullPointerException("Listener cannot be null!");
		}

		this.keyDownListener = listener;
	}

	public final void setKeyUpListener(final IntConsumer listener) {
		if (listener == null) {
			throw new NullPointerException("Listener cannot be null!");
		}

		this.keyUpListener = listener;
	}


	private GameGraphics(final BufferedImage frontBuffer, final BufferedImage backBuffer, final JFrame frame,
	                     final int w, final int h, final boolean vsync) {
		this.frontBuffer = frontBuffer;
		this.backBuffer = backBuffer;
		this.frame = frame;
		this.w = w;
		this.h = h;
		this.vsync = vsync;
	}

	public void setTitle(final String title) {
		frame.setTitle(title);
	}

	public static GameGraphics init(final int w, final int h, final String title,
	                                final Runnable shutdownCallback, final boolean vsync) {
		final BufferedImage frontBuffer = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		final BufferedImage backBuffer = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		{
			final Random random = new Random();
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					frontBuffer.setRGB(x, y, (int) (0xFFFFFF * random.nextDouble()));
					backBuffer.setRGB(x, y, (int) (0xFFFFFF * random.nextDouble()));
				}
			}
		}
		final JFrame frame = new JFrame();
		final GameGraphics result = new GameGraphics(frontBuffer, backBuffer, frame, w, h, vsync);

		frame.addKeyListener(result);
		frame.setResizable(false);
		frame.setTitle(title);

		final JPanel panel = new JPanel() {
			private static final long serialVersionUID = 4469648986423121350L;

			@Override
			public void paintComponent(final Graphics g) {
				synchronized (result) {
					g.drawImage(result.getFrontBuffer(), 0, 0, w, h, 0, 0, w, h, null);
				}
			}
		};
		panel.setPreferredSize(new Dimension(w, h));

		frame.add(panel);
		frame.pack();

		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				shutdownCallback.run();
				result.dispose();
			}
		});

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		return result;
	}

	public final void dispose() {
		synchronized (this) {
			if (!disposed) {
				frame.dispose();
				disposed = true;
			}
		}
	}

	public final Font getFont() {
		return this.font;
	}

	public final void setFont(final Font font) {
		this.font = font.deriveFont((float) FONT_SIZE);
	}

	public final void drawText(final String text, int x, int y, final Color color) {
		synchronized (this) {
			final Graphics g = this.backBuffer.getGraphics();

			g.setColor(color);
			g.setFont(this.font);
			g.drawString(text, x, y + FONT_SIZE);

			g.dispose();
		}
	}

	public final void setPixel(final int x, final int y, final int color) {
		synchronized (this) {
			this.backBuffer.setRGB(x, y, color);
		}
	}

	public final void drawRect(final int x, final int y, final int width, final int height, final Color color) {
		synchronized (this) {
			final Graphics g = this.backBuffer.getGraphics();

			g.setColor(color);
			g.fillRect(x, y, width, height);

			g.dispose();
		}
	}

	public final void flipBuffers() {
		synchronized (this) {
			final BufferedImage tmp = this.backBuffer;
			this.backBuffer = this.frontBuffer;
			this.frontBuffer = tmp;

			if (vsync) {
				toolkit.sync();
			}

			this.frame.repaint();
		}
	}

	@Override
	public final void keyTyped(final KeyEvent e) {	}

	@Override
	public final void keyPressed(KeyEvent e) {
		this.keyDownListener.accept(e.getKeyCode());
	}

	@Override
	public final void keyReleased(KeyEvent e) {
		this.keyUpListener.accept(e.getKeyCode());
	}
}
