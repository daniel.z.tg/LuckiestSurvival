package me.danielzgtg.speedprogramming2017.survival;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Random;

public final class Survival implements Runnable {
	private static final int TUTORIAL_WAIT = 200;
	private static final boolean HIGH_PERFORMANCE = false;
	private static final boolean VSYNC_ENABLED = false;
	private static final boolean DEBUGCHEAT_GODMODE = false;
	private static final boolean DEBUGCHEAT_NOAI = false;
	private static final boolean DEBUGCHEATS_ENABLED = DEBUGCHEAT_GODMODE || DEBUGCHEAT_NOAI;

	private static final Color HEALTH_0_COLOR = Color.BLACK;
	private static final Color HEALTH_1_COLOR = new Color(0x004444);
	private static final Color HEALTH_2_COLOR = new Color(0xFF00FF);
	private static final Color HEALTH_3_COLOR = new Color(0x55DD00);

	private final int width;
	private final int height;
	private int playerPosX;
	private int playerPosY;

	private final Random rng = new Random();
	private GameGraphics graphics;
	private TickTimer timer;
	private volatile boolean running = true;
	private AxisAlignedBB playZoneAABB;
	public final SurvivalSettings gameContext;

	private int aliveTicks;
	private int aliveTicksHigh;
	private int lives;
	private boolean gameOver = false;
	private int deadTicksLeft = 0;

	public final EventType<boolean[]> deathEvent = new EventType<>();
	public final EventType<Integer> livesChangedEvent = new EventType<>();

	private Color clearColor = HEALTH_3_COLOR;

	volatile boolean movingUp = false;
	volatile boolean movingDown = false;
	volatile boolean movingLeft = false;
	volatile boolean movingRight = false;

	private AxisAlignedBB playerAABBCache;
	private int[][] sectorMapCache;
	private boolean cacheDirty = true;
	private int[][] enemies;

	public Survival(final int width, final int height, final SurvivalSettings gameContext) {
		if (((width | height) & 1) != 0) {
			throw new IllegalArgumentException("Screen dimensions must be both even!");
		}

		this.timer = new TickTimer();
		this.gameContext = gameContext;
		this.timer.setPeriod(this.gameContext.tickPeriod);

		this.playZoneAABB = new AxisAlignedBB(0, width, 0, height);
		this.width = width;
		this.height = height;
		this.enemies = new int[this.gameContext.enemies][8];

		this.livesChangedEvent.listeners.add(x -> {
			switch (x) {
				case 0:
					this.clearColor = HEALTH_0_COLOR;
					break;
				case 1:
					this.clearColor = HEALTH_1_COLOR;
					break;
				case 2:
					this.clearColor = HEALTH_2_COLOR;
					break;
				case 3:
					this.clearColor = HEALTH_3_COLOR;
					break;
			}
		});
	}

	public GameGraphics getGraphics() {
		return this.graphics;
	}

	public void shutdown() {
		this.running = false;
	}

	@Override
	public void run() {
		setup();

		this.resetGame();
		while (running) {
			tick();

			final long waitTime = this.gameContext.tickPeriod - timer.peekElapsed();
			if (!HIGH_PERFORMANCE && waitTime > 0) {
				try {
					Thread.sleep(waitTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		this.cleanup();
	}

	@SuppressWarnings("all")
	private final void tick() {
		if (timer.nextTickReady()) {
			this.render();

			if (!gameOver) {
				int ticks = 0;

				TICKLOOP:
				do {
					ticks++;
					aliveTicks++;
					if (movingUp ^ movingDown) {
						if (movingUp) {
							this.playerPosY -= this.gameContext.playerSpeed;
							this.cacheDirty = true;
						} else {
							this.playerPosY += this.gameContext.playerSpeed;
							this.cacheDirty = true;
						}
					}

					if (movingLeft ^ movingRight) {
						if (movingLeft) {
							this.playerPosX -= this.gameContext.playerSpeed;
							this.cacheDirty = true;
						} else {
							this.playerPosX += this.gameContext.playerSpeed;
							this.cacheDirty = true;
						}
					}

					final AxisAlignedBB playerAABB;
					int[][] sectorMap;

					if (this.cacheDirty) {
						this.cacheDirty = false;
						playerAABB = (this.playerAABBCache = new AxisAlignedBB(
								playerPosX - this.gameContext.playerRadius,
								playerPosX + this.gameContext.playerRadius,
								playerPosY - this.gameContext.playerRadius,
								playerPosY + this.gameContext.playerRadius));
						sectorMap = (this.sectorMapCache = generateSectorData(playerAABB));
					} else {
						playerAABB = this.playerAABBCache;
						sectorMap = this.sectorMapCache;
					}


					final int monsterRadius = this.gameContext.monsterRadius;
					for (int[] enemyInfo : enemies) {
						final AxisAlignedBB enemyAABB = new AxisAlignedBB(
								enemyInfo[0] - monsterRadius, enemyInfo[0] + monsterRadius,
								enemyInfo[1] - monsterRadius, enemyInfo[1] + monsterRadius);
						updateEnemy(enemyInfo);

						if (!playZoneAABB.intersects(enemyAABB)) {
							resetEnemy(enemyInfo, playerAABB, sectorMap);
							continue;
						}

						if (playerAABB.intersects(enemyAABB)) {
							resetEnemy(enemyInfo, playerAABB, sectorMap);
							if (!DEBUGCHEAT_GODMODE) {
								int newLives = this.livesChangedEvent.fire(this.lives - 1);
								this.lives = newLives;

								if (newLives == 0) {
									this.die();
									break TICKLOOP;
								}
							} else {
								LogSystem.info("[DEBUG] n00b d3v!");
							}
						}
					}

					if (!playZoneAABB.intersects(playerAABB)) {
						this.die();
						break TICKLOOP;
					}
				} while (timer.nextTickReady());

				if (ticks != 1) {
					LogSystem.err(String.format("[PERFWARN] Tick lag of %3d!", ticks));
				}
			} else {
				timer.reset();

				if (this.deadTicksLeft > 0) {
					if (this.gameContext.deadWait > 2) {
						if (this.deadTicksLeft == (this.gameContext.deadWait / 2)) {
							this.resetGame();
						} else if (this.deadTicksLeft == this.gameContext.deadWait) {
							LogSystem.info("[GAME] gg ez");
						}
					} else {
						if (this.deadTicksLeft == this.gameContext.deadWait) {
							LogSystem.info("[GAME] gg ez");
						}

						this.resetGame();
					}

					this.deadTicksLeft--;
				} else {
					gameOver = false;
				}
			}
		}
	}

	private final void cleanup() {
		LogSystem.info("[CLEANUP] Cleaning up...");
		this.printScore();
		graphics.dispose();

		LogSystem.info("[CLEANUP] Game ended.");
	}

	private final void printScore() {
		if (!DEBUGCHEATS_ENABLED) {
			LogSystem.info(String.format("[SCORE] You were alive for %5d ticks", this.aliveTicks));
		}
	}

	private final void die() {
		final boolean allowed = this.deathEvent.fire(new boolean[]{true})[0];

		if (allowed) {
			printScore();

			if (this.aliveTicksHigh < this.aliveTicks) {
				this.aliveTicksHigh = this.aliveTicks;
			}

			this.lives = this.livesChangedEvent.fire(0);
			this.gameOver = true;
			this.deadTicksLeft = this.gameContext.deadWait;
		}
	}

	private final void setup() {
		LogSystem.info("[SETUP] Welcome to " + this.gameContext.title + "!");

		LogSystem.info("[ENV] Tickrate is " + (1000L / this.gameContext.tickPeriod) + "/s");

		if (DEBUGCHEATS_ENABLED) {
			LogSystem.info("[ENV] [SCORE] Debug cheats are enabled, so score is disabled");
		}

		graphics = GameGraphics.init(width, height, this.gameContext.title, this::shutdown, VSYNC_ENABLED);

		try {
			graphics.setFont(
					Font.createFont(Font.TRUETYPE_FONT,
							this.getClass().getClassLoader().getResourceAsStream("NotoSans-Regular.ttf")));
		} catch (final Exception e) {
			LogSystem.err("[GUI] Failed to load font!");
			throw new RuntimeException(e);
		}

		graphics.setKeyDownListener((x) -> {
			switch (x) {
				case KeyEvent.VK_UP:
					movingUp = true;
					break;
				case KeyEvent.VK_DOWN:
					movingDown = true;
					break;
				case KeyEvent.VK_LEFT:
					movingLeft = true;
					break;
				case KeyEvent.VK_RIGHT:
					movingRight = true;
					break;
				case KeyEvent.VK_W:
					movingUp = true;
					break;
				case KeyEvent.VK_S:
					movingDown = true;
					break;
				case KeyEvent.VK_A:
					movingLeft = true;
					break;
				case KeyEvent.VK_D:
					movingRight = true;
					break;
				case KeyEvent.VK_ESCAPE:
					this.shutdown();
					break;
				case KeyEvent.VK_R:
					this.die();
					break;
			}
		});

		graphics.setKeyUpListener((x) -> {
			switch (x) {
				case KeyEvent.VK_UP:
					movingUp = false;
					break;
				case KeyEvent.VK_DOWN:
					movingDown = false;
					break;
				case KeyEvent.VK_LEFT:
					movingLeft = false;
					break;
				case KeyEvent.VK_RIGHT:
					movingRight = false;
					break;
				case KeyEvent.VK_W:
					movingUp = false;
					break;
				case KeyEvent.VK_S:
					movingDown = false;
					break;
				case KeyEvent.VK_A:
					movingLeft = false;
					break;
				case KeyEvent.VK_D:
					movingRight = false;
					break;
			}
		});

		clearScreen();
		graphics.flipBuffers();
		LogSystem.info("[SETUP] Initialized!");
	}

	private final void render() {
		clearScreen();

		graphics.drawRect(playerPosX - this.gameContext.playerRadius, playerPosY - this.gameContext.playerRadius,
				this.gameContext.playerRadius * 2, this.gameContext.playerRadius * 2,
				Color.RED);

		final int monsterRadius = this.gameContext.monsterRadius;
		for (int[] enemyInfo : enemies) {
			graphics.drawRect(enemyInfo[0] - monsterRadius, enemyInfo[1] - monsterRadius,
					monsterRadius * 2, monsterRadius * 2,
					Color.YELLOW);
		}

		graphics.drawText(this.gameContext.title, 0, 12 * 0, Color.WHITE);

		int yOffset;
		if (aliveTicks < TUTORIAL_WAIT) {
			graphics.drawText("Use the arrow keys or WASD to move. Help the Red Square avoid the Yellow Squares!",
					0, 12 * 2, Color.WHITE);
			graphics.drawText("[ESC] to Exit. [R] to Reset Character.", 0, 12 * 3, Color.WHITE);
			yOffset = 12 * 4;
		} else {
			yOffset = 0;
		}

		graphics.drawText((this.gameOver ? "Your final score is: " : "Score: ") +
				this.aliveTicks, 0, yOffset + 12 * 1, Color.WHITE);
		graphics.drawText("Lives: " + this.lives, 0, yOffset + 12 * 2, Color.WHITE);
		graphics.drawText("Highscore: " + this.aliveTicksHigh, 0, yOffset + 12 * 3, Color.WHITE);

		graphics.flipBuffers();
	}

	private final void updateEnemy(final int[] enemyInfo) {
		if (DEBUGCHEAT_NOAI) {
			return;
		}

		if (enemyInfo[4] <= 0) {
			enemyInfo[0] += enemyInfo[2];
			enemyInfo[1] += enemyInfo[3];

			if (enemyInfo[7] == 0) {
				enemyInfo[2] += enemyInfo[5];
				enemyInfo[3] += enemyInfo[6];
				enemyInfo[7] = this.gameContext.monsterAccelPeriod;
			} else {
				enemyInfo[7]--;
			}
		} else {
			enemyInfo[4]--;
		}
	}

	private final int[][] generateSectorData(final AxisAlignedBB playerAABB) {
		final int monsterlessRadius = this.gameContext.monsterlessRadius;
		final int monsterRadius = this.gameContext.monsterlessRadius;
		/*
		 * 0 1 2
		 * 3 P 4
		 * 5 6 7
		 */
		boolean[] availableSectors = new boolean[8];
		availableSectors[0] = playZoneAABB.surrounds(
				playerAABB.minX - (monsterlessRadius + monsterRadius),
				playerAABB.minY - (monsterlessRadius + monsterRadius));
		availableSectors[2] = playZoneAABB.surrounds(
				playerAABB.maxX + (monsterlessRadius + monsterRadius),
				playerAABB.minY - (monsterlessRadius + monsterRadius));
		availableSectors[5] = playZoneAABB.surrounds(
				playerAABB.minX - (monsterlessRadius + monsterRadius),
				playerAABB.maxY + (monsterlessRadius + monsterRadius));
		availableSectors[7] = playZoneAABB.surrounds(
				playerAABB.maxX + (monsterlessRadius + monsterRadius),
				playerAABB.maxY + (monsterlessRadius + monsterRadius));

		availableSectors[1] = availableSectors[0] && availableSectors[2];
		availableSectors[3] = availableSectors[0] && availableSectors[5];
		availableSectors[4] = availableSectors[2] && availableSectors[7];
		availableSectors[6] = availableSectors[5] && availableSectors[7];

		int[][] sectorMap = new int[9][4];
		int numSectors = 0;
		for (int i = 0; i < 8; i++) {
			if (availableSectors[i]) {
				/*
				 * 0 1 2
				 * 3 P 4
				 * 5 6 7
				 */
				switch (i) {
					case 0:
						sectorMap[numSectors][0] = 1 - monsterRadius;
						sectorMap[numSectors][1] = playerAABB.minX - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][2] = 1 - monsterRadius;
						sectorMap[numSectors][3] = playerAABB.minY - (monsterlessRadius + monsterRadius);
						break;
					case 1:
						sectorMap[numSectors][0] = playerAABB.minX - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][1] = playerAABB.maxX + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][2] = 1 - monsterRadius;
						sectorMap[numSectors][3] = playerAABB.minY - (monsterlessRadius + monsterRadius);
						break;
					case 2:
						sectorMap[numSectors][0] = playerAABB.maxX + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][1] = width + monsterRadius - 1;
						sectorMap[numSectors][2] = 1 - monsterRadius;
						sectorMap[numSectors][3] = playerAABB.minY - (monsterlessRadius + monsterRadius);
						break;
					case 3:
						sectorMap[numSectors][0] = 1 - monsterRadius;
						sectorMap[numSectors][1] = playerAABB.minX - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][2] = playerAABB.minY - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][3] = playerAABB.maxY + (monsterlessRadius + monsterRadius);
						break;
					case 4:
						sectorMap[numSectors][0] = playerAABB.maxX + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][1] = width + monsterRadius - 1;
						sectorMap[numSectors][2] = playerAABB.minY - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][3] = playerAABB.maxY + (monsterlessRadius + monsterRadius);
						break;
					case 5:
						sectorMap[numSectors][0] = 1 - monsterRadius;
						sectorMap[numSectors][1] = playerAABB.minX - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][2] = playerAABB.maxY + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][3] = height + monsterRadius - 1;
						break;
					case 6:
						sectorMap[numSectors][0] = playerAABB.minX - (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][1] = playerAABB.maxX + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][2] = playerAABB.maxY + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][3] = height + monsterRadius - 1;
						break;
					case 7:
						sectorMap[numSectors][0] = playerAABB.maxX + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][1] = width + monsterRadius - 1;
						sectorMap[numSectors][2] = playerAABB.maxY + (monsterlessRadius + monsterRadius);
						sectorMap[numSectors][3] = height + monsterRadius - 1;
						break;
				}

				numSectors++;
			}
		}

		sectorMap[8][0] = numSectors;
		return sectorMap;
	}

	@SuppressWarnings("all")
	private final void resetEnemy(final int[] enemyInfo, final AxisAlignedBB playerAABB, final int[][] sectorMap) {
		final int chosenSector = rng.nextInt(sectorMap[8][0]);

		final int
				minX = sectorMap[chosenSector][0],
				maxX = sectorMap[chosenSector][1],
				minY = sectorMap[chosenSector][2],
				maxY = sectorMap[chosenSector][3];

		int x = minX + rng.nextInt(maxX - minX + 1);
		int y = minY + rng.nextInt(maxY - minY + 1);

		//		final AxisAlignedBB tryAABB = new AxisAlignedBB(
		//				tryX - MONSTERLESS_LENGTH + MONSTER_LENGTH, tryX + MONSTERLESS_LENGTH + MONSTER_LENGTH,
		//				tryY - MONSTERLESS_LENGTH + MONSTER_LENGTH, tryY + MONSTERLESS_LENGTH + MONSTER_LENGTH);

		enemyInfo[0] = x;
		enemyInfo[1] = y;

		if ((this.gameContext.monsterMaxSpeed - this.gameContext.monsterMinSpeed) > 0) {
			enemyInfo[2] = (this.gameContext.monsterMinSpeed +
					rng.nextInt(this.gameContext.monsterMaxSpeed - this.gameContext.monsterMinSpeed))
					* (rng.nextBoolean() ? 1 : -1);
			enemyInfo[3] = (this.gameContext.monsterMinSpeed +
					rng.nextInt(this.gameContext.monsterMaxSpeed - this.gameContext.monsterMinSpeed))
					* (rng.nextBoolean() ? 1 : -1);
		} else {
			enemyInfo[2] = this.gameContext.monsterMinSpeed * (rng.nextBoolean() ? 1 : -1);
			enemyInfo[3] = this.gameContext.monsterMinSpeed * (rng.nextBoolean() ? 1 : -1);
		}

		enemyInfo[4] = 20;

		if ((this.gameContext.monsterMaxAccel - this.gameContext.monsterMinAccel) > 0) {
			enemyInfo[5] = (this.gameContext.monsterMinAccel +
					rng.nextInt(this.gameContext.monsterMaxAccel - this.gameContext.monsterMinAccel))
					* (rng.nextBoolean() ? 1 : -1);
			enemyInfo[6] = (this.gameContext.monsterMinAccel +
					rng.nextInt(this.gameContext.monsterMaxAccel - this.gameContext.monsterMinAccel))
					* (rng.nextBoolean() ? 1 : -1);
		} else {
			enemyInfo[5] = this.gameContext.monsterMinAccel * (rng.nextBoolean() ? 1 : -1);
			enemyInfo[6] = this.gameContext.monsterMinAccel * (rng.nextBoolean() ? 1 : -1);
		}

		enemyInfo[7] = this.gameContext.monsterAccelPeriod;
	}

	private final void clearScreen() {
		final Color color = this.clearColor;

		if (gameOver) {
			final int clearColor = color.getRGB();
			final int
					r = (clearColor & 0xFF0000) >>> 16,
					g = (clearColor & 0x00FF00) >>> 8,
					b = clearColor & 0x0000FF,
					washOut = (int) ((((this.gameContext.deadWait / 2.0) - Math.abs((this.gameContext.deadWait / 2.0)
							- this.deadTicksLeft)) / (this.gameContext.deadWait / 2.0)) * 0xFF);

			graphics.drawRect(0, 0, width, height,
					new Color((Math.min(r + washOut, 0xFF) << 16)
							+ (Math.min(g + washOut, 0xFF) << 8)
							+ Math.min(b + washOut, 0xFF)));
		} else {
			graphics.drawRect(0, 0, width, height, color);
		}
	}

	private final void resetGame() {
		LogSystem.info("[GAME] Resetting...");

		playerPosX = width / 2;
		playerPosY = height / 2;

		final AxisAlignedBB playerAABB = new AxisAlignedBB(
				playerPosX - this.gameContext.playerRadius, playerPosX + this.gameContext.playerRadius,
				playerPosY - this.gameContext.playerRadius, playerPosY + this.gameContext.playerRadius);
		this.cacheDirty = true;

		for (int[] enemyInfo : this.enemies) {
			resetEnemy(enemyInfo, playerAABB, generateSectorData(playerAABB));
		}

		this.lives = this.livesChangedEvent.fire(3);
		this.aliveTicks = 0;
		this.timer.reset();
	}
}
