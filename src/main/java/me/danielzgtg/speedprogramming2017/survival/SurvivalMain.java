package me.danielzgtg.speedprogramming2017.survival;

public final class SurvivalMain {
	public static final SurvivalSettings GAME_TYPE = new SurvivalSettings(
			"Survival of the Luckiest",
			17,
			10,
			50,
			3,
			1,
			5,
			0,
			2,
			20,
			120,
			15,
			5
	);

	public static final void main(String[] args) {
		final Survival game = new Survival(640, 480, GAME_TYPE);

//		game.deathEvent.listeners.add(x -> {x[0] = false;});

		new Thread(game).start();
	}

	private SurvivalMain() {
		throw new UnsupportedOperationException();
	}
}
