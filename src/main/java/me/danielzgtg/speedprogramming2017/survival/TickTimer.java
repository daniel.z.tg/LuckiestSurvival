package me.danielzgtg.speedprogramming2017.survival;

public final class TickTimer {
	private long lastTime, period;

	public TickTimer() {
		this.period = 1L;
	}

	public TickTimer(final long period) {
		this.period = period;
	}

	private TickTimer(final long period, final long lastTime) {
		this.lastTime = lastTime;
		this.period = period;
	}

	public final long getPeriod() {
		return period / (1000L * 1000L);
	}

	public final void setPeriod(final long period) {
		if (period < 1) {
			throw new IllegalArgumentException("A zero period is not meaningful!");
		}

		this.period = period * (1000L * 1000L);
	}

	public final void reset() {
		this.lastTime = System.nanoTime();
	}

	public final long peekElapsedFast() {
		return (System.nanoTime() - this.lastTime) / (1000L * 1000L);
	}

	public final long peekElapsed() {
		return ((System.nanoTime() - this.lastTime) / (1000L * 1000L)) * (1000L * 1000L);
	}

	public final long getElapsedFast() {
		return (-this.lastTime + (this.lastTime = System.nanoTime())) / (1000L * 1000L);
	}

	public final long getElapsed() {
		final long reported = ((System.nanoTime() - this.lastTime) / (1000L * 1000L)) * (1000L * 1000L);
		this.lastTime += reported;

		return reported;
	}

	public final boolean nextTickReady() {
		if ((System.nanoTime() - this.lastTime) >= period) {
			this.lastTime += period;
			return true;
		}

		return false;
	}

	public final boolean peekTickReady() {
		return (System.nanoTime() - this.lastTime) >= period;
	}

	@Override
	public final int hashCode() {
		return Long.hashCode(this.lastTime) ^ Long.hashCode(period);
	}

	@Override
	public final boolean equals(final Object o) {
		if (!(o instanceof TickTimer)) {
			return false;
		}

		final TickTimer other = (TickTimer) o;
		return this.lastTime == other.lastTime && this.period == other.period;
	}

	@Override
	public final String toString() {
		return "TickTimer: { period=" + this.period + ", this.lastTime=" + this.lastTime + " }";
	}

	@Override
	public final TickTimer clone() {
		return new TickTimer(period, this.lastTime);
	}
}
