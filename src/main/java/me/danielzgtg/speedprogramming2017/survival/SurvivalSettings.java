package me.danielzgtg.speedprogramming2017.survival;

public class SurvivalSettings {

	public final String title;
	public final int tickPeriod;
	public final int enemies;
	public final int monsterlessRadius;
	public final int monsterRadius;
	public final int monsterMinSpeed;
	public final int monsterMaxSpeed;
	public final int monsterMinAccel;
	public final int monsterMaxAccel;
	public final int monsterAccelPeriod;
	public final int deadWait;
	public final int playerRadius;
	public final int playerSpeed;

	public SurvivalSettings(final String title, final int tickPeriod, final int enemies,
	                        final int monsterlessRadius, final int monsterRadius,
	                        final int monsterMinSpeed, final int monsterMaxSpeed,
	                        final int monsterMinAccel, final int monsterMaxAccel,
	                        final int monsterAccelPeriod, final int deadWait,
	                        final int playerRadius, final int playerSpeed) {
		this.title = title;
		this.tickPeriod = tickPeriod;
		this.enemies = enemies;
		this.monsterlessRadius = monsterlessRadius;
		this.monsterRadius = monsterRadius;
		this.monsterMinSpeed = monsterMinSpeed;
		this.monsterMaxSpeed = monsterMaxSpeed;
		this.monsterMinAccel = monsterMinAccel;
		this.monsterMaxAccel = monsterMaxAccel;
		this.monsterAccelPeriod = monsterAccelPeriod;
		this.deadWait = deadWait;
		this.playerRadius = playerRadius;
		this.playerSpeed = playerSpeed;
	}
}
