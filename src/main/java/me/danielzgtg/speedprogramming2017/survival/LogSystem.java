package me.danielzgtg.speedprogramming2017.survival;

import java.util.Calendar;

public final class LogSystem {

	public static final void info(final Object o) {
		System.out.println(getTimePrefix() + "[INFO] " + o.toString());
	}

	public static final void err(final Object o) {
		System.err.println(getTimePrefix() + "[ERR] " + o.toString());
	}

	private static final String getTimePrefix() {
		final Calendar cal = Calendar.getInstance();

		return String.format("[%02d:%02d:%02d %02d/%02d/%04d] ",
				cal.get(Calendar.HOUR_OF_DAY),
				cal.get(Calendar.MINUTE),
				cal.get(Calendar.SECOND),
				cal.get(Calendar.DAY_OF_MONTH),
				cal.get(Calendar.MONTH),
				cal.get(Calendar.YEAR)
		);
	}

	private LogSystem() {
		throw new UnsupportedOperationException();
	}
}
