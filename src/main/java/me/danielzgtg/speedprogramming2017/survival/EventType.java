package me.danielzgtg.speedprogramming2017.survival;

import java.util.LinkedList;
import java.util.List;

public class EventType<D> {

	public List<EventListener<D>> listeners = new LinkedList<>();

	public D fire(D data) {
		listeners.forEach(x -> x.onEvent(data));
		return data;
	}
}
