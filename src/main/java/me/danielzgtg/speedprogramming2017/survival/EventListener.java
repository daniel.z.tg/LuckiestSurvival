package me.danielzgtg.speedprogramming2017.survival;

@FunctionalInterface
public abstract interface EventListener<D> {

	public abstract void onEvent(D data);
}
