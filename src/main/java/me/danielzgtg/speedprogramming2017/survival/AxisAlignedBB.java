package me.danielzgtg.speedprogramming2017.survival;

public final class AxisAlignedBB {
	public final int minX, maxX, minY, maxY;

	public AxisAlignedBB(final int minX, final int maxX, final int minY, final int maxY) {
		if ((maxX < minX) || (maxY < minY)) {
			throw new IllegalArgumentException();
		}

		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
	}

	public final AxisAlignedBB grow(final int add) {
		try {
			return new AxisAlignedBB(this.minX - add, this.maxX + add, this.minY - add, this.maxY + add);
		} catch (final IllegalArgumentException iae) {
			return this.getMidpointAABB();
		}
	}

	public final AxisAlignedBB grow(final int minX, final int maxX, final int minY, final int maxY) {
		try {
			return new AxisAlignedBB(this.minX - minX, this.maxX + maxX, this.minY - minY, this.maxY + maxY);
		} catch (final IllegalArgumentException iae) {
			return this.getMidpointAABB();
		}
	}

	public final AxisAlignedBB scale(final int scale) {
		try {
			return new AxisAlignedBB(this.minX * scale, this.maxX * scale, this.minY * scale, this.maxY * scale);
		} catch (final IllegalArgumentException iae) {
			return this.getMidpointAABB();
		}
	}

	public final AxisAlignedBB scale(final int minXScale, final int maxXScale, final int minYScale, final int maxYScale) {
		try {
			return new AxisAlignedBB(this.minX * minXScale, this.maxX * maxXScale, this.minY * minYScale, this.maxY * maxYScale);
		} catch (final IllegalArgumentException iae) {
			return this.getMidpointAABB();
		}
	}

	public final int getMidpointX() {
		return (this.minX + this.maxX) / 2;
	}

	public final int getMidpointY() {
		return (this.minY + this.maxY) / 2;
	}

	public final AxisAlignedBB getMidpointAABB() {
		final int x = (this.minX + this.maxX) / 2,
				y = (this.minY + this.maxY) / 2;
		return new AxisAlignedBB(x, x, y, y);
	}

	public final int[] getMidpoint() {
		final int[] result = new int[2];

		result[0] = (this.minX + this.maxX) / 2;
		result[1] = (this.minY + this.maxY) / 2;

		return result;
	}

	public final AxisAlignedBB normalize() {
		final int x = (this.minX + this.maxX) / 2,
				y = (this.minY + this.maxY) / 2;

		return new AxisAlignedBB(x - 1, x + 1, y - 1, y + 1);
	}

	public final int getArea() {
		return (this.maxX - this.minX) * (this.maxY - this.minY);
	}

	public final boolean intersects(final AxisAlignedBB other) {
		//		if (other == null) throw new NullPointerException("No way to check against a null other AABB!");

		return (this.minX <= other.maxX) && (this.maxX >= other.minX) && (this.minY <= other.maxY) && (this.maxY >= other.minY);
	}

	public final boolean intersects(final int minX, final int maxX, final int minY, final int maxY) {
		return ((minX <= maxX) && (minY <= maxY)) &&
				(this.minX <= maxX) && (this.maxX >= minX) && (this.minY <= maxY) && (this.maxY >= minY);
	}

	public static final boolean intersects(final int minX1, final int maxX1, final int minY1, final int maxY1,
	                                       final int minX2, final int maxX2, final int minY2, final int maxY2) {
		return ((minX1 <= maxX1) && (minY1 <= maxY1)) && ((minX2 <= maxX2) && (minY2 <= maxY2)) &&
				(minX1 <= maxX2) && (maxX1 >= minX2) && (minY1 <= maxY2) && (maxY1 >= minY2);
	}

	public final boolean surrounds(final int x, final int y) {
		return (((x >= this.minX) && (x <= this.maxX)) && ((y >= this.minY) && (y <= this.maxY)));
	}

	public static final boolean surrounds(final int minX, final int maxX, final int minY, final int maxY,
	                                      final int x, final int y) {
		return ((minX <= maxX) && (minY <= maxY)) && (((x >= minX) && (x <= maxX)) && ((y >= minY) && (y <= maxY)));
	}

	public final boolean surrounds(final int minX, final int maxX, final int minY, final int maxY) {
		return ((minX <= maxX) && (minY <= maxY)) &&
				(((minX >= this.minX) && (maxX <= this.maxX)) && ((minY >= this.minY) && (maxY <= this.maxY)));
	}

	public final boolean surrounds(final AxisAlignedBB other) {
		return (((other.minX >= this.minX) && (other.maxX <= this.maxX)) && ((other.minY >= this.minY) && (other.maxY <= this.maxY)));
	}

	public static final boolean surrounds(final int minX1, final int maxX1, final int minY1, final int maxY1,
	                                      final int minX2, final int maxX2, final int minY2, final int maxY2) {
		return ((minX1 <= maxX1) && (minY1 <= maxY1)) && ((minX2 <= maxX2) && (minY2 <= maxY2)) &&
				(((minX2 >= minX1) && (maxX2 <= maxX1)) && ((minY2 >= minY1) && (maxY2 <= maxY1)));
	}

	public final int getMinX() {
		return minX;
	}

	public final int getMaxX() {
		return maxX;
	}

	public final int getMinY() {
		return minY;
	}

	public final int getMaxY() {
		return maxY;
	}

	@Override
	public final int hashCode() {
		return this.minX ^ this.maxX ^ this.minY ^ this.maxY;
	}

	@Override
	public final boolean equals(final Object o) {
		if (!(o instanceof AxisAlignedBB)) {
			return false;
		}

		final AxisAlignedBB other = (AxisAlignedBB) o;
		return (this.minX == other.minX) && (this.maxX == other.maxX) &&
				(this.minY == other.minY) && (this.maxY == other.maxY);
	}

	@Override
	public final String toString() {
		return "AABB: { minX=" + this.minX +
				", maxX=" + this.maxX +
				", minY=" + this.minY +
				", maxY=" + this.maxY + " }";
	}

	@Override
	public final AxisAlignedBB clone() {
		return this;
	}
}
